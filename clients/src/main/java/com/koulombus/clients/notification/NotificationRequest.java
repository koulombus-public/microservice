package com.koulombus.clients.notification;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class NotificationRequest {

    @Id
    private String toCustomerId;
    private String toCustomerName;
    private String message;
}
