package com.koulombus.notification.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NotificationConfig {

    @Value("${rabbitmq.exchanges.internal}")
    public String internalExchange;

    @Value("${rabbitmq.routing-keys.internal-notification}")
    public String internalNotificationRoutingKey;

    @Value("${rabbitmq.queue.notification}")
    public String notificationQueue;

    // config of the exchange, check picture rabbitMQ overview.jpg
    @Bean
    public TopicExchange internalTopicExchange() {
        return new TopicExchange(this.internalExchange);
    }

    // config of the binding, check picture rabbitMQ overview.jpg
    @Bean
    public Binding internalToNotificationBinding() {
        return BindingBuilder.bind(notificationQueue()) //
                             .to(internalTopicExchange()) //
                             .with(this.internalNotificationRoutingKey);
    }

    // config of the queue, check picture rabbitMQ overview.jpg
    @Bean
    public Queue notificationQueue() {
        return new Queue(this.notificationQueue);
    }
}
