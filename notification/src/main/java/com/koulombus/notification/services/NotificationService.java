package com.koulombus.notification.services;

import java.time.Instant;

import com.koulombus.clients.notification.NotificationRequest;
import com.koulombus.notification.model.NotificationModel;
import com.koulombus.notification.model.NotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NotificationService {
    private static final String SENDER = "Koulombus";
    private final NotificationRepository notificationRepository;

    public void send(NotificationRequest notificationRequest) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setToCustomerId(notificationRequest.getToCustomerId());
        notificationModel.setToCustomerEmail(notificationRequest.getToCustomerName());
        notificationModel.setMessage(notificationRequest.getMessage());
        notificationModel.setSender(SENDER);
        notificationModel.setSentAt(Instant.now());
        notificationRepository.save(notificationModel);
    }
}
