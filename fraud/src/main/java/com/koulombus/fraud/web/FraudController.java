package com.koulombus.fraud.web;

import com.koulombus.fraud.services.FraudCheckService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/fraud-check")
@AllArgsConstructor
public class FraudController {

    private final FraudCheckService fraudCheckService;

    @GetMapping(path = "/customer-id/{customerId}")
    public Boolean isFraudster(@PathVariable("customerId") String customerId) {
        log.info("Fraud check request for customer: {}", customerId);
        return fraudCheckService.setFraudulentCustomer(customerId);
    }
}
