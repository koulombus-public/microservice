package com.koulombus.fraud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

// Inject RabbitMQProducer with the scanBasePackages
@SpringBootApplication(scanBasePackages = {"com.koulombus.fraud", "com.koulombus.amqp"})
@PropertySources({
        @PropertySource("classpath:clients-${spring.profiles.active}.properties")
})
//@EnableEurekaClient
public class FraudApplication {
    public static void main(String[] args) {
        SpringApplication.run(FraudApplication.class, args);
    }
}
