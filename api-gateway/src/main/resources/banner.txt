  ,---.   ,------.  ,--.          ,----.      ,---.   ,--------. ,------. ,--.   ,--.   ,---.   ,--.   ,--.
 /  O  \  |  .--. ' |  | ,-----. '  .-./     /  O  \  '--.  .--' |  .---' |  |   |  |  /  O  \   \  `.'  /
|  .-.  | |  '--' | |  | '-----' |  | .---. |  .-.  |    |  |    |  `--,  |  |.'.|  | |  .-.  |   '.    /
|  | |  | |  | --'  |  |         '  '--'  | |  | |  |    |  |    |  `---. |   ,'.   | |  | |  |     |  |
`--' `--' `--'      `--'          `------'  `--' `--'    `--'    `------' '--'   '--' `--' `--'     `--'

Application Name: ${application.title}
Application Version: ${application.version}
${AnsiColor.BLUE}:: Spring Boot ::                ${spring-boot.formatted-version}${AnsiColor.BLUE}
${AnsiColor.DEFAULT}
