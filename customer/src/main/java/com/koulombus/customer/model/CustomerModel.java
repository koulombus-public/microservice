package com.koulombus.customer.model;

import java.util.UUID;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// The @Document annotation is the Spring Data annotation that marks this class as defining a
// MongoDB document data model.
@Document
@Data
public class CustomerModel {

    @Id
    private String id = UUID.randomUUID().toString();
    private String firstName;
    private String lastName;
    private String email;
}
