package com.koulombus.customer.services;

import java.util.List;

import com.koulombus.amqp.RabbitMqMessageProducer;
import com.koulombus.clients.fraud.FraudClient;
import com.koulombus.clients.notification.NotificationRequest;
import com.koulombus.customer.configuration.CustomerConfig;
import com.koulombus.customer.model.CustomerModel;
import com.koulombus.customer.model.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class CustomerService {

    @Autowired
    private CustomerConfig customerConfig;

    private final CustomerRepository customerRepository;
    private final FraudClient fraudClient;
    private final RabbitMqMessageProducer rabbitMqMessageProducer;

    public CustomerModel registerCustomer(CustomerModel customer) {
        log.info("Save new customer.");
        CustomerModel newCustomer = customerRepository.save(customer);
        log.info("Check if customer is fraudster....");
        Boolean response = fraudClient.isFraudster(newCustomer.getId());

        if (Boolean.TRUE.equals(response)) {
            throw new IllegalStateException("fraudster");
        }

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setToCustomerId(newCustomer.getId());
        notificationRequest.setToCustomerName(newCustomer.getEmail());
        notificationRequest.setMessage(String.format("Hi %s, welcome to microservice...", newCustomer.getFirstName()));

        rabbitMqMessageProducer.publish(notificationRequest, //
                                        customerConfig.internalExchange, //
                                        customerConfig.internalNotificationRoutingKey);

        return newCustomer;
    }

    public List<CustomerModel> findAllCustomers() {
        return customerRepository.findAll();
    }
}
