package com.koulombus.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

// Inject RabbitMQProducer with the scanBasePackages
@SpringBootApplication(
        scanBasePackages = {"com.koulombus.customer", "com.koulombus.amqp"})
@PropertySources({
        @PropertySource("classpath:clients-${spring.profiles.active}.properties")
})
//@EnableEurekaClient
@EnableFeignClients(basePackages = "com.koulombus.clients")
public class CustomerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerApplication.class, args);
    }
}
