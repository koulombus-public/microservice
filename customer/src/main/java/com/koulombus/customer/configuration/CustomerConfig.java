package com.koulombus.customer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CustomerConfig {

    @Value("${rabbitmq.exchanges.internal}")
    public String internalExchange;

    @Value("${rabbitmq.routing-keys.internal-notification}")
    public String internalNotificationRoutingKey;

    /**
     * The annotation Loadbalancer is to give spring the information that they are maybe more than one Fraud service
     * is running.
     *
     * @return RestTemplate
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
